<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Product;
use App\Entity\Basket;
use App\Entity\User;

class BasketController extends AbstractController
{
    #[Route('/user/basket', name: 'basket')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $basket = $doctrine->getRepository(Basket::class)->findBy(['User' => $this->getUser()]);

        // $product = [];
        // $products = [];

        // foreach ($basket as $item) {
        //     $object = new object;
        //     $object->id = $item->getId();
        //     $object->name = $item->getTitle();
        //     $object->price = $item->getPrice();
        //     $object->image = $item->getImage();
        //     $object->quantity = 1;

        //     if ( in_array( $object, $product) ){

        //     }
        // }

        return $this->render('basket/index.html.twig', [
            'product' => $basket,
        ]);
    }

    #[Route('/user/addbasket/{id}', name: 'addbasket')]
    public function add(int $id, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $product = $doctrine->getRepository(Product::class)->find($id);

        if ( $product->getStock() > 0){
        $basket = new Basket();
        $basket->setProduct($product);
        $basket->setUser($this->getUser());

        $entityManager->persist($basket);
        $entityManager->flush();

        return $this->redirectToRoute('basket');
        }

        return $this->redirectToRoute('product', array(
            'id' => $id,
            'alert' => 'alert',
        ));
    }
}
