<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Form\ImageFormType;

class ImageController extends AbstractController
{
    #[Route('/admin/image', name: 'image')]
    public function index(Request $request,EntityManagerInterface $entityManager): Response
    {

        $form = $this->createForm(ImageFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->get('images')->getData();
            if ( $image ){
                // On hash le nom de l'image pour éviter des conflits d'intérêt
            $fichier = md5(uniqid()). '.'.$image->guessExtension();

            // On stocke l'image dans le dossier public/uploads
            $image->move(
                $this->getParameter("images_directory"),
                $fichier
            );
            }

            return $this->renderForm('image/link.html.twig', [
                'link' => '/../img/'.$fichier
            ]);
        }

        

        return $this->render('image/index.html.twig', [
            'formRender' => $form->createView(),
        ]);
    }
}
