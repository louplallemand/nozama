<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\Product;
use App\Entity\Tag;

class TagProductController extends AbstractController
{
    #[Route('/admin/tag', name: 'tag')]
    public function index(ManagerRegistry $doctrine, PaginatorInterface $paginator, EntityManagerInterface $em, Request $request): Response
    {
        $tag = $doctrine->getRepository(Tag::class)->findAll();

        $dql = "SELECT a FROM App:Product a ORDER BY a.id DESC";
        $query = $em->createQuery($dql);
    
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('tag_product/index.html.twig', [
            'pagination' => $pagination,
            'tag' => $tag,
        ]);
    }

    #[Route('/admin/addtag/{id}/{tag}', name: 'addtag')]
    public function add (int $tag, int $id, ManagerRegistry $doctrine) {
        $entityManager = $doctrine->getManager();
        $product = $doctrine->getRepository(Product::class)->find($id);
        $tag = $doctrine->getRepository(Tag::class)->find($tag);
        $tags = [];
        foreach( $product->getTag() as $item ){
            array_push($tags, $item);
        }

        if (!in_array($tag, $tags)){
            $product->AddTag($tag);

            $entityManager->persist($product);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tag');
        
    }
}
