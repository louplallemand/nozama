<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\Product;
use App\Entity\Tag;
use App\Form\SearchType;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(ManagerRegistry $doctrine, EntityManagerInterface $em, PaginatorInterface $paginator, Request $request): Response
    {
        $form = $this->createForm(SearchType::class);

        $form->handleRequest($request);
        $text = $form->get('text')->getData();

        if ($form->isSubmitted() && $form->isValid() && $text !== null) {
            $tag = $doctrine->getRepository(Tag::class)->findBy(["name" => $text]);
            if($tag){
                $products = $doctrine->getRepository(Tag::class)->findBy(["name" => $text])[0]->getProduct();               

                $pagination = $paginator->paginate(
                    $products, /* query NOT result */
                    $request->query->getInt('page', 1), /*page number*/
                    10 /*limit per page*/
                );

                return $this->renderForm('home/index.html.twig', [
                    'pagination' => $pagination,
                    'formRender' => $form,
                    'search' => "Resultats pour : {$text}",
                ]);
            }

            $dql = 
                "SELECT a
                FROM App:Product a 
                WHERE a.title LIKE :text
                ORDER BY a.id DESC ";
            $query = $em->createQuery($dql)->setParameter('text', '%'.$text.'%');

            $pagination = $paginator->paginate(
                $query, /* query NOT result */
                $request->query->getInt('page', 1), /*page number*/
                10 /*limit per page*/
            );

            return $this->renderForm('home/index.html.twig', [
                'pagination' => $pagination,
                'formRender' => $form,
                'search' => "Resultats pour : {$text}",
            ]);
        }

        $dql = "SELECT a FROM App:Product a ORDER BY a.id DESC";
        $query = $em->createQuery($dql);
    
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('home/index.html.twig', [
            'pagination' => $pagination,
            'formRender' => $form->createView(),
            'search' => 'Tout les résultats',
        ]);
    }

}
