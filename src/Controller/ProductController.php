<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Product;
use App\Entity\Comment;

class ProductController extends AbstractController
{
    #[Route('/product/{id}/{alert}', name: 'product')]
    public function index(string $alert,int $id, ManagerRegistry $doctrine): Response
    {
        $product = $doctrine->getRepository(Product::class)->findById($id);
        $comments = $doctrine->getRepository(Comment::class)->findBy(["Product" => $id]);

        return $this->render('product/index.html.twig', [
            'product' => $product[0],
            'comments' => $comments,
            'alert' => $alert,
        ]);
    }
}
