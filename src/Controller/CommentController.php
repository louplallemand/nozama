<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Product;
use App\Entity\Comment;
use App\Entity\User;
use App\Form\CommentType;

class CommentController extends AbstractController
{
    #[Route('/user/addcomment/{id}', name: 'addcomment')]
    public function add(int $id, ManagerRegistry $doctrine, Request $request): Response
    {
        $entityManager = $doctrine->getManager();

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $doctrine->getRepository(Product::class)->find($id);
            $comment->setProduct($product);
            $comment->setUser($this->getUser());

            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('product', array(
                'id' => $id,
                'alert' => 'ref'
            ));
        }

        return $this->render('comment/index.html.twig', [
            'formRender' => $form->createView(),
        ]);
    }
}
